using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    public Transform cameraFollowTarget;
    public CinemachineVirtualCamera cinemachineCamera;
    
    [SerializeField] private float movementSpeed = 25f;
    [SerializeField] private Vector3 jumpPosition;
    [SerializeField] private float jumpDuration = 1f;
    [SerializeField] private float rotationPerSecond = 45f;
    [SerializeField] private float screenOffsetDetection = 0.05f;
    [SerializeField] private Vector2 zoomLevels = new Vector2(10, 35);
    [SerializeField] private float currentZoom;
    [SerializeField] private float zoomSpeed = 5f;
    
    [SerializeField] private Vector2 _currentInput;
    [SerializeField] private float _currentRotationInput;
    [SerializeField] private bool _jumpedThisFrame;
    [SerializeField] private float _currenScroll;
    [SerializeField] private float smoothVelocity;
    
    // Default values
    private Vector3 defaultRotation;
    private Vector3 defaultPosition;

    public float minX = -10;
    public float minZ = -10;
    public float maxX = 200;
    public float maxZ = 10;
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        
        Gizmos.DrawLine(new Vector3(minX, 0, maxZ), new Vector3(minX, 0, minZ));
        Gizmos.DrawLine(new Vector3(minX, 0, maxZ), new Vector3(maxX, 0, maxZ));
        Gizmos.DrawLine(new Vector3(minX, 0, minZ), new Vector3(maxX, 0, minZ));
        Gizmos.DrawLine(new Vector3(maxX, 0, maxZ), new Vector3(maxX, 0, minZ));
    }

    private void Start()
    {
        defaultPosition = cameraFollowTarget.position;
        defaultRotation = cameraFollowTarget.eulerAngles;
        jumpPosition = cameraFollowTarget.position;
        currentZoom = cinemachineCamera.m_Lens.OrthographicSize;
    }

    public void ResetCamera()
    {
        cameraFollowTarget.position = defaultPosition;
        cameraFollowTarget.eulerAngles = defaultRotation;
    }
    
    public void ResetCameraRotation()
    {
        cameraFollowTarget.eulerAngles = defaultRotation;
    }

    public void Move(InputAction.CallbackContext context)
    {
        _currentInput = context.ReadValue<Vector2>();
    }
    
    public void Rotate(InputAction.CallbackContext context)
    {
        _currentRotationInput = context.ReadValue<float>();
    }
    
    public void Scroll(InputAction.CallbackContext context)
    {
        int multiplier = Settings.InvertScroll ? -1 : 1;
        _currenScroll = context.ReadValue<float>() * multiplier;
    }
    
    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _jumpedThisFrame = true;
        }
    }

    public void SetJumpPosition(Vector3 jumpPosition)
    {
        this.jumpPosition = new Vector3(jumpPosition.x, 0, jumpPosition.z);
    }

    void Update()
    {
        if (_jumpedThisFrame)
        {
            cameraFollowTarget.position = jumpPosition;
            _jumpedThisFrame = false;
        }
        
        if (_currentRotationInput != 0)
        {
            RotateCamera();
        }
        
        if (_currentInput.sqrMagnitude > 0)
        {
            MoveCamera();
        }
        else if (Settings.UseCornerZoom)
        {
            MoveCameraMouse();
        }
        
        Zoom();
    }

    void Zoom()
    {
        if (_currenScroll != 0)
        {
            currentZoom += _currenScroll * zoomSpeed;
            currentZoom = Mathf.Clamp(currentZoom, zoomLevels.x, zoomLevels.y);
        }
        cinemachineCamera.m_Lens.OrthographicSize = Mathf.SmoothDamp(cinemachineCamera.m_Lens.OrthographicSize, currentZoom, ref smoothVelocity, 0.25f);
    }
    
    void MoveCameraMouse()
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;
        Vector2 mousePosition = Mouse.current.position.ReadValue();
        Vector3 movementVector = new Vector3(0, 0, 0).normalized;

        
        if (mousePosition.x >= screenWidth * (1 - screenOffsetDetection))
        {
            movementVector.x = 1;
        }
        else if (mousePosition.x <= screenWidth * screenOffsetDetection)
        {
            movementVector.x = -1;
        }
        
        if (mousePosition.y >= screenHeight * (1 - screenOffsetDetection))
        {
            movementVector.z = 1;
        }
        else if (mousePosition.y <= screenHeight * screenOffsetDetection)
        {
            movementVector.z = -1;
        }
        
        MoveCam(movementVector);
    }

    void MoveCamera()
    {
        MoveCam(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
    }

    private void MoveCam(Vector3 movementVector)
    {
        Vector3 movement = Quaternion.Euler(0, cameraFollowTarget.eulerAngles.y ,0) * movementVector.normalized * (Time.deltaTime * movementSpeed);
        Vector3 position = cameraFollowTarget.position;
        position += movement;
        position.x = Mathf.Clamp(position.x, minX, maxX);
        position.z = Mathf.Clamp(position.z, minZ, maxZ);
        cameraFollowTarget.position = position;
    }

    void RotateCamera()
    {
        int multiplier = Settings.InvertRotation ? -1 : 1;

        if (_currentRotationInput > 0)
        {
            RotateDegrees(multiplier * rotationPerSecond);
        }
        else
        {
            RotateDegrees(multiplier * -1 * rotationPerSecond);
        }
    }

    private void RotateDegrees(float rotationDegrees)
    {
        Vector3 rotation = cameraFollowTarget.eulerAngles;
        rotation.y += rotationDegrees * Time.deltaTime;
        cameraFollowTarget.eulerAngles = rotation;
    }
}
