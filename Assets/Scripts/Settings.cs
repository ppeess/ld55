using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Settings
{
    private static bool initialized = false;

    public static void Initialize()
    {
        if (PlayerPrefs.HasKey("invertScroll"))
        {
            invertScroll = (PlayerPrefs.GetInt("invertScroll") == 1);
        }
        if (PlayerPrefs.HasKey("invertRotation"))
        {
            invertRotation = (PlayerPrefs.GetInt("invertRotation") == 1);
        }
        if (PlayerPrefs.HasKey("useCornerZoom"))
        {
            useCornerZoom = (PlayerPrefs.GetInt("useCornerZoom") == 1);
        }
    }

    private static bool invertScroll;
    public static bool InvertScroll
    {
        get
        {
            if (!initialized)
            {
                Initialize();
            }
            return invertScroll;
        }
        set
        {
            invertScroll = value;
            PlayerPrefs.SetInt("invertScroll", (value == true ? 1 : 0));
            PlayerPrefs.Save();
        }
    }
    
    private static bool invertRotation;
    public static bool InvertRotation
    {
        get
        {
            if (!initialized)
            {
                Initialize();
            }
            return invertRotation;
        }
        set
        {
            invertRotation = value;
            PlayerPrefs.SetInt("invertRotation", (value == true ? 1 : 0));
            PlayerPrefs.Save();
        }
    }
    
    private static bool useCornerZoom;
    public static bool UseCornerZoom
    {
        get
        {
            if (!initialized)
            {
                Initialize();
            }
            return useCornerZoom;
        }
        set
        {
            useCornerZoom = value;
            PlayerPrefs.SetInt("useCornerZoom", (value == true ? 1 : 0));
            PlayerPrefs.Save();
        }
    }
}
