using System;
using Stage;
using Units.Combat;
using UnityEngine;
using UnityEngine.AI;
using Utils;

public class UnitMovement : MonoBehaviour
{
    public Lane lane;
    public bool canMove = true;
    public float timeUntilMoving;
    public float timeUntilMovingBuffer = 0.1f;
    private NavMeshAgent _agent;
    private Damageable _damageable;
    [SerializeField] private Transform moveTarget;
    [SerializeField] private Faction faction;
    [SerializeField] private bool initialized = false;

    public void Initialize(Lane lane, Faction faction)
    {
        this.faction = faction;
        this.lane = lane;
        initialized = true;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _damageable = GetComponent<Damageable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!initialized)
        {
            return;
        }
        
        if (moveTarget == null)
        {
            // Default back to next target in lane
            
            Target nextTarget = lane.GetNextTarget(faction);
            if (nextTarget != null)
            {
                moveTarget = nextTarget.transform;
            }
            
            if (moveTarget == null)
            {
                canMove = false;
                _agent.isStopped = true;
                initialized = false;
                return;
            }
        }

        if (timeUntilMoving > 0f)
        {
            timeUntilMoving -= Time.deltaTime;
            if (timeUntilMoving <= 0f)
            {
                _agent.isStopped = false;
                canMove = true;
            }
            else
            {
                _agent.isStopped = true;
            }
        }

        _agent.speed = _damageable.stats.GetStat(StatType.Speed);
        _agent.destination = moveTarget.position;
        Debug.DrawRay(transform.position, moveTarget.position - transform.position, Color.blue);
    }

    public void MoveToTarget(Transform target)
    {
        moveTarget = target;
    }

    public void SetAttackTarget(Transform currentTarget, float abilityPresetUseDuration)
    {
        MoveToTarget(currentTarget);
        canMove = false;
        timeUntilMoving = abilityPresetUseDuration + timeUntilMovingBuffer;
    }
}
