﻿using System.Collections.Generic;
using Assets.Scripts.Stats;
using UnityEngine;

namespace Units.Combat.Action
{
    [CreateAssetMenu(fileName = "Direct Action", menuName = "Actions/Direct Action Preset")]
    public class DirectAction : Action
    {
        public GameObject impactVFX;
        public float impactVFXDuration = 5f;

        public DamageInstance impactDamage;
        public List<StatModifier> impactModifiers;
        
        public override void Use(UseData useData)
        {
            if (useData.target != null && useData.abilityCaster != null)
            {
                useData.target.Hit(useData.abilityCaster, impactDamage, impactModifiers);
                GameObject instance = Instantiate(impactVFX, useData.target.transform);
                Debug.DrawRay(useData.target.transform.position, useData.abilityCaster.transform.transform.position - useData.target.transform.position, Color.green, 5f);
                Destroy(instance, impactVFXDuration);
            }
        }
    }
}