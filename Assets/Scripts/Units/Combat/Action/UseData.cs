﻿using UnityEngine;

namespace Units.Combat.Action
{
    public class UseData
    {
        public Source abilityCaster;
        public Ability ability;
        public Damageable target;
        public Vector3 targetPosition;
        public bool used = true;
    }
}