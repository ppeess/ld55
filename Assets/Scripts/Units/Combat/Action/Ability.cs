﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Units.Combat.Action
{
    [Serializable]
    public class Ability
    {
        private readonly float _chargeCooldown;
        private readonly float _cooldown;

        private readonly bool _hasCharges;        
        private readonly bool _requireTarget;        
        private readonly bool _requireTargetPosition;        
        private readonly List<AbilityTarget> _possibleTargets;
        private readonly int _maxCharges;        
        private readonly bool _restoreAtOnce;
        public AbilityPreset abilityPreset;
        private bool _isRecharging;
        private float _remainingChargeCooldown;
        private int _remainingCharges;
        private float _remainingCooldown;
        private Action _useAction;

        public Ability(AbilityPreset abilityPreset)
        {
            _possibleTargets = abilityPreset.abilityTarget;
            this.abilityPreset = abilityPreset;
            _hasCharges = abilityPreset.hasCharges;
            _maxCharges = abilityPreset.chargeData.charges;
            _remainingCharges = abilityPreset.chargeData.charges;
            _restoreAtOnce = abilityPreset.chargeData.restoreAtOnce;
            _cooldown = abilityPreset.cooldown;
            _chargeCooldown = abilityPreset.chargeData.cooldownTimeBetweenCharges;
            _useAction = abilityPreset.useAction;
        }

        public void Use(Source source, UseData useData)
        {
            if (!CanBeUsed(source))
            {
                Debug.LogError("Using ability even though it cannot be used!");
            }
            
            _useAction.Use(useData);

            if (_hasCharges)
            {
                _remainingCharges -= 1;
                _remainingChargeCooldown = _chargeCooldown;
                _isRecharging = true;
                _remainingCooldown = _cooldown;
            }
            else
            {
                _isRecharging = true;
                _remainingCooldown = _cooldown;
            }
        }

        public void Update(float deltaTime)
        {
            _remainingChargeCooldown -= deltaTime;
    
            if (_isRecharging)
            {
                _remainingCooldown -= deltaTime;
                if (_remainingCooldown <= 0)
                {
                    if (_hasCharges)
                    {
                        if (_restoreAtOnce)
                        {
                            _remainingCharges = _maxCharges;
                            _isRecharging = false;
                        }
                        else
                        {
                            _remainingCharges += 1;
                            if (_remainingCharges < _maxCharges)
                                _remainingChargeCooldown = _chargeCooldown;
                            else
                                _isRecharging = false;
                        }
                    }
                    else
                    {
                        _isRecharging = false;
                    }
                }
                if (_remainingChargeCooldown <= 0) { }
            }
        }

        public bool CanBeUsed(Source abilityCaster)
        {
            // TODO: introduce logic from the caster
            if (_hasCharges) return _remainingCharges > 0 && _remainingChargeCooldown <= 0;

            return !_isRecharging;
        }
    }
}