﻿using System.Collections.Generic;
using UnityEngine;


namespace Units.Combat.Action
{
    [CreateAssetMenu(fileName = "Ability Preset", menuName = "Ability/Ability Preset")]
    public class AbilityPreset : ScriptableObject
    {
        public int priority = 5;
        public TargetPriority targetPriority;
        public TargetSearchType targetSearchType;
        public float useRange = 10f;
        public float useDuration = 0.5f;
        public float targetSearchRange = 10f;
        public List<AbilityTarget> abilityTarget;
        public string abilityName;
        public string abilityDescription;
        public float cooldown;
        public bool hasCharges;
        public ChargeData chargeData;
        public Action useAction;
    }
}

public enum TargetSearchType
{
    Unit,
    Point
}

public enum TargetPriority
{
    Closest,
    Furthest,
    Lowest,
    Building
}