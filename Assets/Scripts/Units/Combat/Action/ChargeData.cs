﻿using System;

namespace Units.Combat.Action
{
    [Serializable]
    public class ChargeData {
        public bool restoreAtOnce = false;
        public int charges;
        public float cooldownTimeBetweenCharges;
    }
}