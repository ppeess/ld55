﻿using System.Collections.Generic;
using Assets.Scripts.Stats;
using UnityEngine;

namespace Units.Combat.Action.ActionObjects
{
    public class Projectile : MonoBehaviour
    {
        public GameObject impactVFX;
        public float impactVFXDuration = 5f;
        public GameObject impactEffect;
        public DamageInstance impactDamage;
        public List<StatModifier> impactModifiers;
        public float speed = 10f;
        public float projectileDuration = 10f;
        private float _remainingProjectileDuration = 0f;
        [SerializeField]
        private Source source;
        [SerializeField]
        private string sourceTag;

        public bool isHoming = false;    
        public Transform target;
        private Vector3 _direction;
        private UseData _useData;

        private void Start(){
            _remainingProjectileDuration = projectileDuration;
        }

        public void Initialize(UseData useData)
        {
            source = useData.abilityCaster;
            
            if (isHoming)
            {
                target = useData.target.transform;
            }
            else if (useData.ability.abilityPreset.targetSearchType == TargetSearchType.Point)
            {
                _direction = useData.targetPosition - transform.position;
            }
            else
            {
                _direction = transform.forward;
            }
        }

        private void Update() {
            _remainingProjectileDuration -= Time.deltaTime;
            if(_remainingProjectileDuration <= 0){
                Destroy(gameObject);
            }
            else {
                Move();
            }
        }

        private void Move(){
            if(!isHoming){
                transform.position = transform.position + Time.deltaTime * _direction;
            }
            else {
                Vector3 dir = target.position - transform.position;
                float distanceThisFrame = speed * Time.deltaTime;
                transform.Translate(dir.normalized * distanceThisFrame, Space.World);
                transform.LookAt(transform.position + dir);
            }
        }

        private void OnTriggerEnter(Collider other) {
            if(other.CompareTag("Collectible") || other.gameObject.CompareTag("Projectile") || other.gameObject.GetComponentInParent<Source>() == source || other.gameObject.GetComponentInParent<Source>() == source){
                return;
            }

            Damageable target = other.gameObject.GetComponent<Damageable>();
            if(other.gameObject.GetComponent<Damageable>()){
                if(source != null && other.gameObject == source.gameObject){
                    return;
                }
                
                // Debug.Log("Other gameObject has Damageable" + other.gameObject.tag);
                if(TargetUtils.CanHitEntity(source, target, _useData.ability.abilityPreset.abilityTarget)){
                    // Debug.Log("Target is valid, source: " + sourceTag + ", target: " + other.gameObject.tag);
                    target.Hit(source, impactDamage, impactModifiers);
                }
                else {
                    // Debug.Log("Target is not valid, source: " + sourceTag + ", target: " + other.gameObject.tag);
                    return;
                }
            }
            Destroy(gameObject);
        }

        private void OnDestroy() {
            if(impactVFX != null){
                GameObject impactVFXInstance = (GameObject)Instantiate(impactVFX, transform.position, transform.rotation);
                Destroy(impactVFXInstance, impactVFXDuration);
            }
            if(impactEffect != null){
                Instantiate(impactEffect, transform.position, transform.rotation);
            }
        }


    }
}