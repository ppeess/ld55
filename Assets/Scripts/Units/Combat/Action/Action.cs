﻿using UnityEngine;

namespace Units.Combat.Action
{
    public abstract class Action : ScriptableObject
    {
        public abstract void Use(UseData useData);
    }
}