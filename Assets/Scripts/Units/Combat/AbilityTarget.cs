﻿namespace Units.Combat
{
    public enum AbilityTarget
    {
        Ally,
        AllyUnit,
        Enemy,
        EnemyUnit,
        Indiscriminate,
        IndiscriminateWithoutStructure
    }
}