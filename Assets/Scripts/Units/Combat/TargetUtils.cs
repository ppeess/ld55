﻿using System.Collections.Generic;
using Assets.Scripts.Stats;
using Units.Combat.Action;
using UnityEngine;
using Utils;

namespace Units.Combat
{
    public class TargetUtils
    {
        public static Damageable GetTarget(Source source, AbilityPreset abilityPreset)
        {
            List<Damageable> possibleTargets = GetTargetsInRange(source, abilityPreset);
            switch (abilityPreset.targetPriority)
            {
                case TargetPriority.Closest:
                    return FindClosest(source, possibleTargets);
                case TargetPriority.Furthest:
                    return FindFurthest(source, possibleTargets);
                case TargetPriority.Lowest:
                    return FindFurthest(source, possibleTargets);
                case TargetPriority.Building:
                    List<Damageable> buildingTargets = possibleTargets.FindAll(damageable => damageable.isStructure);
                    if (buildingTargets.Count > 0)
                    {
                        return FindClosest(source, buildingTargets);
                    }
                    return FindClosest(source, possibleTargets);
            }
            return null;
        }
        
        public static List<Damageable> GetTargetsInRange(Source source, AbilityPreset abilityPreset){
            Collider[] hitColliders = Physics.OverlapSphere(source.transform.position, abilityPreset.targetSearchRange);  
            List<Damageable> inRange = new();
        
            foreach(Collider collider in hitColliders){
                RaycastHit hit;
                // if(TargetUtils.CanHitEntity(source, collider.tag, possibleTargets) && (!Physics.Raycast(source.transform.position, collider.transform.position - source.transform.position, out hit, Vector3.Distance(collider.transform.position, source.transform.position)))){
                Damageable asDamageable = collider.gameObject.GetComponentInParent<Damageable>();
                if(asDamageable != null && CanHitEntity(source, asDamageable, abilityPreset.abilityTarget)){
                    Debug.DrawRay(source.transform.position, collider.transform.position - source.transform.position, Color.red, 2f);
                    inRange.Add(asDamageable);
                }
            }
            
            return inRange;
        }
        
        public static Damageable FindClosest(Source source, List<Damageable> damageables)
        {
            if (damageables == null || damageables.Count == 0)
            {
                return null;
            }
            
            Vector3 position = source.transform.position;
            Damageable closest = null;
            float closestDistance = float.MaxValue;
            foreach (Damageable damageable in damageables)
            {
                float distance = Vector3.Distance(damageable.transform.position, position);
                if (distance <= closestDistance)
                {
                    closest = damageable;
                    closestDistance = distance;
                }
            }
            return closest;
        }
        
        public static Damageable FindFurthest(Source source, List<Damageable> damageables)
        {
            if (damageables == null || damageables.Count == 0)
            {
                return null;
            }
            
            Vector3 position = source.transform.position;
            Damageable furthest = null;
            float longestDistance = float.MinValue;
            foreach (Damageable damageable in damageables)
            {
                float distance = Vector3.Distance(damageable.transform.position, position);
                if (distance >= longestDistance)
                {
                    furthest = damageable;
                    longestDistance = distance;
                }
            }
            return furthest;
        }
        
        public static Damageable FindLowest(Source source, List<Damageable> damageables)
        {
            if (damageables == null || damageables.Count == 0)
            {
                return null;
            }
            
            Vector3 position = source.transform.position;
            Damageable closest = null;
            float lowest = int.MinValue;
            foreach (Damageable damageable in damageables)
            {
                float missingHp = damageable.stats.GetStat(StatType.HP, StatSubValueType.Max) - damageable.stats.GetStat(StatType.HP);
                if (missingHp > lowest)
                {
                    closest = damageable;
                    lowest = missingHp;
                }
            }
            return closest;
        }

        public static bool CanHitEntity(Source source, Damageable target, List<AbilityTarget> possibleTargets)
        {
            if (possibleTargets.Contains(AbilityTarget.Indiscriminate))
            {
                return true;
            }
            if (possibleTargets.Contains(AbilityTarget.IndiscriminateWithoutStructure) && !target.isStructure)
            {
                return true;
            }
            if (possibleTargets.Contains(AbilityTarget.Ally))
            {
                return target.faction == Faction.Ally;
            }
            if (possibleTargets.Contains(AbilityTarget.AllyUnit) && !target.isStructure)
            {
                return target.faction == Faction.Ally;
            }
            if (possibleTargets.Contains(AbilityTarget.Enemy))
            {
                return target.faction == Faction.Enemy;
            }
            if (possibleTargets.Contains(AbilityTarget.EnemyUnit) && !target.isStructure)
            {
                return target.faction == Faction.Enemy;
            }
            return false;
        }
    }
}