﻿namespace Assets.Scripts.Stats
{
    public enum StatSubValueType
    {
        Current,
        Min, 
        Max
    }
}