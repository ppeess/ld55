[System.Serializable]
public class Stat
{
    public StatType statType;
    public int baseValue;
    public int maxValue;
    public int minValue;
    public bool hasMax;
    public bool hasMin;
}

public enum StatType
{
    HP,
    Speed,
    Defense, 
    PierceDefense,
    CrushDefense,
    MagicDefense,
    Regeneration
}