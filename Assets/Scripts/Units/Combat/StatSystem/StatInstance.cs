﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Stats
{
    [Serializable]
    public class StatInstance
    {
        public StatType statType;
        public float baseValue;
        public float currentValue;
        public StatSubValueType statSubValueType;
        public List<StatModifier> statModifiers;

        [SerializeField] private StatInstance maxStat = null;
        [SerializeField] private StatInstance minStat = null;

        public StatInstance(Stat stat)
        {
            statType = stat.statType;
            baseValue = stat.baseValue;
            currentValue = baseValue;
            statSubValueType = StatSubValueType.Current;
            statModifiers = new List<StatModifier>();

            if (stat.hasMax)
            {
                maxStat = new StatInstance(statType, stat.maxValue, StatSubValueType.Max);
            }

            if (stat.hasMin)
            {
                minStat = new StatInstance(statType, stat.minValue, StatSubValueType.Min);
            }

            if (maxStat == null || minStat == null) return;

            minStat.SetMaxStat(maxStat);
            maxStat.SetMinStat(minStat);
        }

        public StatInstance(StatType statType, int baseValue, StatSubValueType statSubValueType)
        {
            this.statType = statType;
            this.baseValue = baseValue;
            currentValue = baseValue;
            this.statSubValueType = statSubValueType;
            statModifiers = new List<StatModifier>();
        }

        void SetMinStat(StatInstance minStat)
        {
            this.minStat = minStat;
        }

        void SetMaxStat(StatInstance maxStat)
        {
            this.maxStat = maxStat;
        }

        public float Get()
        {
            return currentValue;
        }

        public float Get(StatSubValueType subValueType)
        {
            if (subValueType == statSubValueType)
            {
                return Get();
            }
            
            if (subValueType == StatSubValueType.Max && maxStat != null)
            {
                return maxStat.Get();
            }
            
            if (subValueType == StatSubValueType.Min && minStat != null)
            {
                return minStat.Get();
            }

            Debug.LogError(statType + " cannot access " + subValueType);
            return 0;
        }

        void Recalculate()
        {
            float newCurrent = baseValue;
            int percentageModifier = 100;
            int percentageTotalModifier = 100;
            
            foreach (StatModifier statModifier in statModifiers)
            {
                if (statModifier.modifierType == ModifierType.Percentage)
                {
                    percentageModifier += statModifier.value;
                }
                else if (statModifier.modifierType == ModifierType.PercentageTotal)
                {
                    percentageTotalModifier += statModifier.value;
                }
                else
                {
                    newCurrent += statModifier.value;
                }
            }

            newCurrent = Mathf.CeilToInt(newCurrent * ((float)percentageModifier/100));
            newCurrent = Mathf.CeilToInt(newCurrent * ((float)percentageTotalModifier/100));
            
            if (maxStat != null)
            {
                newCurrent = Mathf.Min(maxStat.Get(), currentValue);
            }
            if (minStat != null)
            {
                newCurrent = Mathf.Max(minStat.Get(), currentValue);
            }

            currentValue = newCurrent;
        }

        public void AddModifier(StatModifier statModifier)
        {
            if (statModifier.changedSubValue == statSubValueType)
            {
                statModifiers.Add(statModifier);
            }
            
            else if (statModifier.changedSubValue == StatSubValueType.Max && maxStat != null)
            {
                maxStat.AddModifier(statModifier);
            }
            
            else if (statModifier.changedSubValue == StatSubValueType.Min && minStat != null)
            {
                minStat.AddModifier(statModifier);
            }

            Recalculate();
        }
        
        public void RemoveModifier(StatModifier statModifier)
        {
            if (statModifier.changedSubValue != statSubValueType)
            {
                statModifiers.RemoveAt(statModifiers.IndexOf(statModifiers.Find(modifier => modifier.statModiferId == statModifier.statModiferId)));
            }
            
            if (statModifier.changedSubValue == StatSubValueType.Max && maxStat != null)
            {
                maxStat.RemoveModifier(statModifier);
            }
            
            if (statModifier.changedSubValue == StatSubValueType.Min && minStat != null)
            {
                minStat.RemoveModifier(statModifier);
            }
            
            Recalculate();
        }

        public void ModifyDirectly(float value, StatSubValueType subValueType)
        {
            if (subValueType == statSubValueType)
            {
                currentValue += value;
                if (maxStat != null)
                {
                    currentValue = Mathf.Min(maxStat.currentValue, currentValue);
                }
                if (minStat != null)
                {
                    currentValue = Mathf.Max(minStat.currentValue, currentValue);
                }
            }
            
            else if (subValueType == StatSubValueType.Max && maxStat != null)
            {
                maxStat.ModifyDirectly(value, subValueType);
            }
            
            else if (subValueType == StatSubValueType.Min && minStat != null)
            {
                minStat.ModifyDirectly(value, subValueType);
            }
        }
    }
}