﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Stats
{
    [Serializable]
    public class Stats
    {
        public StatPreset statPreset;
        public List<StatInstance> stats = new List<StatInstance>();

        public void Init()
        {
            stats = new List<StatInstance>();
            foreach (Stat stat in statPreset.stats)
            {
                stats.Add(new StatInstance(stat));
            }
        }

        public void ModifyStat(StatType statType, float value, float multiplier = 1, StatSubValueType statSubValueType = StatSubValueType.Current)
        {
            StatInstance stat = FindStat(statType);
            if (stat != null)
            {
                stat.ModifyDirectly(value * multiplier, statSubValueType);
            }
        }
        
        public void AddStatModifier(StatModifier statModifier)
        {
            StatInstance stat = FindStat(statModifier.statType);
            if (stat != null)
            {
                stat.AddModifier(statModifier);
            }
        }
        
        public void RemoveStatModifier(StatModifier statModifier)
        {
            StatInstance stat = FindStat(statModifier.statType);
            if (stat != null)
            {
                stat.RemoveModifier(statModifier);
            }
        }
        
        public float GetStat(StatType statType, StatSubValueType subValueType = StatSubValueType.Current)
        {
            StatInstance stat = FindStat(statType);
            if (stat == null)
            {
                Debug.LogError("Stat " + statType + " not found. " + subValueType);
                return 0;
            }
            else
            {
                return stat.Get(subValueType);
            }
        }

        public StatInstance FindStat(StatType statName)
        {
            foreach (StatInstance stat in stats)
            {
                if (stat.statType == statName)
                {
                    return stat;
                }
            }
            Debug.LogError("Stat " + statName + " not found.");
            return null;
        }
    }
}