﻿using System;

namespace Assets.Scripts.Stats
{
    [Serializable]
    public class StatModifier
    {
        public string statModiferId = Guid.NewGuid().ToString();
        public StatType statType;
        public int value;
        public ModifierType modifierType;
        public StatSubValueType changedSubValue;
    }
}