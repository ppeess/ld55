﻿namespace Assets.Scripts.Stats
{
    public enum ModifierType
    {
        Flat,
        Percentage,
        PercentageTotal
    }
}