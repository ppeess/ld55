﻿namespace Units.Combat
{
    public enum DamageType
    {
        Strike,
        Crush,
        Pierce,
        Magic
    }
}