﻿using System;

namespace Units.Combat
{
    [Serializable]
    public class DamageInstance
    {
        public DamageType damageType;
        public int value;
    }
}