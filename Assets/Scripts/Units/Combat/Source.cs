﻿using System;
using System.Collections.Generic;
using System.Linq;
using Units.Combat.Action;
using UnityEditor;
using UnityEngine;
using Utils;

namespace Units.Combat
{
    public class Source : MonoBehaviour
    {
        public List<AbilityPreset> abilityPresets;
        public List<Ability> abilities = new List<Ability>();
        public Faction faction;

        private void OnDrawGizmosSelected()
        {
            foreach (AbilityPreset abilityPreset in abilityPresets)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, abilityPreset.useRange);
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(transform.position, abilityPreset.targetSearchRange);
            }
        }

        private void Update()
        {
            foreach (Ability ability in abilities)
            {
                ability.Update(Time.deltaTime);
            }
        }

        private void Start()
        {
            Damageable damageable = gameObject.GetComponent<Damageable>();
            if (damageable != null)
            {
                faction = damageable.faction;
            }
            foreach (AbilityPreset abilityPreset in abilityPresets)
            {
                abilities.Add(new Ability(abilityPreset));
            }
            abilities = abilities.OrderByDescending(ability => ability.abilityPreset.priority).ToList();
        }

        public UseData TryUsingAbilities()
        {
            foreach (Ability ability in abilities)
            {
                UseData useData = UseIfUsable(ability);
                if (useData != null)
                {
                    return useData;
                }
            }
            return null;
        }

        public UseData UseIfUsable(Ability ability)
        {
            if (!ability.CanBeUsed(this))
            {
                return null;
            }
            
            UseData useData = new()
            {
                ability = ability,
                abilityCaster = this
            };
            
            if (ability.abilityPreset.targetSearchType == TargetSearchType.Unit)
            {
                Damageable target = TargetUtils.GetTarget(this, ability.abilityPreset);
                if (target != null)
                {
                    
                    useData.target = target;
                    if (Vector3.Distance(useData.target.transform.position, transform.position) <= ability.abilityPreset.useRange)
                    {
                        Use(ability, useData);
                        return useData;
                    }

                    useData.used = false;
                    return useData;
                }
                return null;
            }
            else {
                Damageable target = TargetUtils.GetTarget(this, ability.abilityPreset);
                if (target != null)
                {
                    useData.target = target;
                    useData.targetPosition = target.transform.position;
                    if (Vector3.Distance(useData.targetPosition, transform.position) <= ability.abilityPreset.useRange)
                    {
                        Use(ability, useData);
                        return useData;
                    }
                    useData.used = false;
                    return useData;
                }
                return null;
            }
        }

        public void Use(Ability ability, UseData useData)
        {
            ability.Use(this, useData);
        }
    }
}