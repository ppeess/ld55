using System;
using System.Collections.Generic;
using Assets.Scripts.Stats;
using UnityEngine;
using Utils;

namespace Units.Combat
{
    public class Damageable : MonoBehaviour
    {
        public Faction faction;
        public bool isStructure;
        public Stats stats;

        public float regenInterval = 5f;
        public float remainingUntilRegen = 5f;
        
        private void Start()
        {
            if (GetComponent<UnitController>() != null)
            {
                faction = GetComponent<UnitController>().faction;
            }
            stats.Init();
        }

        private void Update()
        {
            if (regenInterval > 0)
            {
                remainingUntilRegen -= Time.deltaTime;
                if (remainingUntilRegen <= 0)
                {
                    stats.ModifyStat(StatType.HP, stats.GetStat(StatType.Regeneration));
                    remainingUntilRegen = regenInterval;
                }   
            }
        }

        public void Hit(Source source, DamageInstance impactDamage, List<StatModifier> impactModifiers)
        {
            foreach (StatModifier statModifier in impactModifiers)
            {
                stats.AddStatModifier(statModifier);
            }

            if (impactDamage != null && impactDamage.value != 0)
            {
                float damageReduction = 0;
                switch (impactDamage.damageType)
                {
                    case DamageType.Strike:
                        damageReduction = stats.GetStat(StatType.Defense);
                        break;
                    case DamageType.Crush:
                        damageReduction = stats.GetStat(StatType.CrushDefense);
                        break;
                    case DamageType.Pierce:
                        damageReduction = stats.GetStat(StatType.PierceDefense);
                        break;
                    case DamageType.Magic:
                        damageReduction = stats.GetStat(StatType.MagicDefense);
                        break;
                }
                float damage = Mathf.Max(impactDamage.value - damageReduction, 1);

                if (stats.FindStat(StatType.HP) != null)
                {
                    stats.ModifyStat(StatType.HP, damage, -1);
                    if (isStructure && faction == Faction.Ally)
                    {
                        GameManager.instance.cameraController.SetJumpPosition(transform.position);
                    }
                    CheckDeath(source);
                }
            }
        }

        public void CheckDeath(Source source)
        {
            if (stats.GetStat(StatType.HP) <= 0)
            {
                Die(source);
            }
        }

        private void Die(Source killer)
        {
            Destroy(gameObject);
        }
    }
}
