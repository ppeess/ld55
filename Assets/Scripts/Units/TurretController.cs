﻿using System;
using Units.Combat;
using Units.Combat.Action;
using UnityEngine;

namespace Units
{
    public class TurretController : MonoBehaviour
    {
        public Source source;
        
        public float remainingActionCooldown = 0f;
        public float checkUsableAbilitiesInterval = 0.25f;
        public float remainingCheckCooldown = 0f;

        private void Start()
        {
            source = gameObject.GetComponent<Source>();
        }

        private void Update()
        {
            if (remainingCheckCooldown > 0)
            {
                remainingCheckCooldown -= Time.deltaTime;
            }
            
            if (remainingActionCooldown > 0)
            {
                remainingActionCooldown -= Time.deltaTime;
                if (remainingActionCooldown > 0) 
                {
                    return;
                }
            }

            if (remainingCheckCooldown <= 0)
            {
                remainingCheckCooldown = checkUsableAbilitiesInterval;
                UseData result = source.TryUsingAbilities();
                if (result != null)
                {
                    if (result.used)
                    {
                        remainingActionCooldown = result.ability.abilityPreset.useDuration;
                    }
                }
            }
        }
    }
}