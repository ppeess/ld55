﻿using System;
using System.Collections.Generic;
using Economy;
using Stage;
using Units.Combat;
using Units.Combat.Action;
using UnityEngine;
using Utils;

namespace Units
{
    public class UnitController : MonoBehaviour
    {
        public Faction faction;
        public Lane lane;
        public UnitMovement unitMovement;
        public Source source;
        public Transform currentTarget = null;
        
        public float remainingActionCooldown = 0f;
        public float checkUsableAbilitiesInterval = 0.25f;
        public float remainingCheckCooldown = 0f;

        public bool initialized = false;
        private bool _wasInitializedBefore = false;

        public Sprite buildIcon;
        public List<Resource> unitCost;
        public List<Resource> killReward;
        
        public void Initialize(Lane unitLane)
        {
            unitMovement = gameObject.GetComponent<UnitMovement>();
            source = gameObject.GetComponent<Source>();
            lane = unitLane;
            if (unitMovement != null)
            {
                unitMovement.Initialize(unitLane, faction);
            }
            
            initialized = true;
            
            GameManager manager = GameManager.instance;
            if (manager != null)
            {
                GameObject gameObject = manager.uiHolder.GetUiElement(UiAssignmentValue.Healthbar);
                if (gameObject != null)
                {
                    Instantiate(gameObject, transform);
                }
            }
        }

        public void Update()
        {
            if (!initialized)
            {
                return;
            }
            if (!_wasInitializedBefore)
            {
                if (unitMovement == null)
                {
                    unitMovement = gameObject.GetComponent<UnitMovement>();
                }
                
                if (unitMovement != null)
                {
                    unitMovement.Initialize(lane, faction);
                }
                
                _wasInitializedBefore = true;
            }
            
            if (remainingCheckCooldown > 0)
            {
                remainingCheckCooldown -= Time.deltaTime;
            }
            
            if (remainingActionCooldown > 0)
            {
                remainingActionCooldown -= Time.deltaTime;
                if (remainingActionCooldown > 0) 
                {
                    return;
                }
            }

            if (remainingCheckCooldown <= 0)
            {
                remainingCheckCooldown = checkUsableAbilitiesInterval;
                UseData result = source.TryUsingAbilities();
                if (result != null)
                {
                    currentTarget = result.target.transform;

                    if (result.used)
                    {
                        unitMovement.SetAttackTarget(currentTarget, result.ability.abilityPreset.useDuration);
                        remainingActionCooldown = result.ability.abilityPreset.useDuration;
                    }
                    else
                    {
                        unitMovement.canMove = true;
                        unitMovement.MoveToTarget(currentTarget);
                    }
                }
            }

            if (currentTarget == null)
            {
                unitMovement.canMove = true;
            }
        }

        private void OnDestroy()
        {
            if (faction == Faction.Enemy)
            {
                ResourceManager.instance.Add(killReward);
            }
        }
    }
}