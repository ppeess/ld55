﻿using System;
using Stage;
using Units;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{
    public class SpawnManager : MonoBehaviour
    {
        public UnitController testEnemy;
        public UnitController testRangedEnemy;
        public UnitController testAlly;
        public UnitController testRangedAlly;
        public Lane testLane;
        public float cooldown = 5f;
        public float remainingCooldown;


        private void Awake()
        {
            testLane.Initialize();
        }

        private void Update()
        {
            if (remainingCooldown <= 0)
            {
                Spawn(testEnemy, testLane);
                Spawn(testAlly, testLane);
                Spawn(testRangedAlly, testLane);
                Spawn(testRangedEnemy, testLane);
                remainingCooldown = cooldown;
            }
            else
            {
                remainingCooldown -= Time.deltaTime;
            }
        }
        
        public void Spawn(UnitController unitController, Lane lane)
        {
            Target target = lane.GetCurrentSpawn(unitController.faction);
            Transform spawnLocation = target.spawnPoints[Random.Range(0, target.spawnPoints.Count)];
            GameObject unit = Instantiate(unitController.gameObject, spawnLocation.position, Quaternion.identity);
            unit.GetComponent<UnitController>().Initialize(lane);
        }
    }
}