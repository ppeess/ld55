using System;
using System.Collections;
using System.Collections.Generic;
using Stage;
using Units.Combat;
using UnityEngine;
using Utils;

public class Target : MonoBehaviour
{
    public Faction faction;
    public List<Lane> subscribers;
    public List<Transform> spawnPoints;

    public void InitUI()
    {
        Instantiate(GameManager.instance.uiHolder.GetUiElement(UiAssignmentValue.BuildUi), transform);
    }

    private void OnDestroy()
    {
        foreach (Lane lane in subscribers)
        {
            lane.TargetDestroyed(this);
        }
    }
}
