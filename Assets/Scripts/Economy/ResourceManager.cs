﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Economy
{
    public class ResourceManager : MonoBehaviour
    {
        public static ResourceManager instance;

        public Resource bones;
        public Resource giantBones;
        public Resource crystals;

        public int bonesIncome;
        public int giantBonesIncome;
        public int crystalIncome;

        public float resourceGainInterval = 5f;
        public float timeUntilResources = 5f;
        
        private void Awake()
        {
            instance = this;
            
            bones.resourceType = ResourceType.Bone;
            giantBones.resourceType = ResourceType.GiantBone;
            crystals.resourceType = ResourceType.Crystal;
        }

        private void Update()
        {
            timeUntilResources -= Time.deltaTime;
            if (timeUntilResources <= 0f)
            {
                bones.amount += bonesIncome;
                giantBones.amount += giantBonesIncome;
                crystals.amount += crystalIncome;
                timeUntilResources = resourceGainInterval;
            }
        }

        public bool CanPurchase(List<Resource> costs)
        {
            foreach (Resource cost in costs)
            {
                if (!CanPurchase(cost))
                {
                    return false;
                }
            }
            return true;
        }
        
        public bool CanPurchase(Resource cost)
        {
            switch (cost.resourceType)
            {
                case ResourceType.GiantBone:
                    return cost.amount <= giantBones.amount;
                case ResourceType.Crystal:
                    return cost.amount <= crystals.amount;
                default:
                    return cost.amount <= bones.amount;
            }
        }
        
        public void Add(List<Resource> toAdd)
        {
            foreach (Resource addition in toAdd)
            {
                Add(addition);
            }
        }

        public void Add(Resource toAdd)
        {
            switch (toAdd.resourceType)
            {
                case ResourceType.GiantBone:
                    giantBones.amount += toAdd.amount;
                    break;
                case ResourceType.Crystal:
                    crystals.amount += toAdd.amount;
                    break;
                default:
                    bones.amount += toAdd.amount;
                    break;
            }
        }
        
        public void Purchase(List<Resource> costs)
        {
            foreach (Resource cost in costs)
            {
                Purchase(cost);
            }
        }

        public void Purchase(Resource cost)
        {
            switch (cost.resourceType)
            {
                case ResourceType.GiantBone:
                    giantBones.amount -= cost.amount;
                    break;
                case ResourceType.Crystal:
                    crystals.amount -= cost.amount;
                    break;
                default:
                    bones.amount -= cost.amount;
                    break;
            }
        }
        
        public int Get(ResourceType resourceType)
        {
            switch (resourceType)
            {
                case ResourceType.GiantBone:
                    return giantBones.amount;
                case ResourceType.Crystal:
                    return crystals.amount;
                default:
                    return bones.amount;
            }
        }
    }
}