﻿using System;

namespace Economy
{
    [Serializable]
    public class Resource
    {
        public ResourceType resourceType;
        public int amount;
    }
}