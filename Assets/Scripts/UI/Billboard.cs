﻿using UnityEngine;

namespace UI
{
    public class Billboard : MonoBehaviour
    {
        private Transform camPos;
        
        void Start() 
        {
            camPos = Camera.main.transform;
            GetComponentInChildren<Canvas>().worldCamera = Camera.main;
        }

        void LateUpdate()
        {
            this.transform.LookAt(this.transform.position + camPos.forward);
        }
    }
}