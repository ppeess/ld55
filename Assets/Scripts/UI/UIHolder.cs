﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    [CreateAssetMenu(fileName = "UI", menuName = "UI Holder")]
    public class UIHolder : ScriptableObject
    {
        public List<UiAssignment> uiAssignments;

        public GameObject GetUiElement(UiAssignmentValue uiAssignmentValue)
        {
            UiAssignment assignment = uiAssignments.Find(element => element.name == uiAssignmentValue);
            if (assignment != null)
            {
                return assignment.uiElement;
            }
            return null;
        }
    }
}

[Serializable]
public class UiAssignment
{
    public UiAssignmentValue name;
    public GameObject uiElement;
}

public enum UiAssignmentValue
{
    Healthbar,
    BuildUi,
    BuildButton
}