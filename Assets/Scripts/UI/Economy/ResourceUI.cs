﻿using System;
using Economy;
using TMPro;
using UnityEngine;

namespace UI.Economy
{
    public class ResourceUI : MonoBehaviour
    {
        public ResourceType resourceType;
        public ResourceManager resourceManager;
        public TMP_Text value;
        
        private void Start()
        {
            resourceManager = ResourceManager.instance;
        }

        private void Update()
        {
            value.text = resourceManager.Get(resourceType).ToString();
        }
    }
}