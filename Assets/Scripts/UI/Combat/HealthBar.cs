﻿using Assets.Scripts.Stats;
using Units.Combat;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils;

namespace DefaultNamespace
{
    public class HealthBar : MonoBehaviour
    {
        public Image filling;
        public Image white;
        public Damageable damageable;
        public float lerpSpeed = 1f;

        public float currHP;
        public float maxHP;

        public void Start()
        {
            damageable = GetComponentInParent<Damageable>();
            if (damageable.faction == Faction.Enemy)
            {
                filling.color = Color.red;
            }
        }

        private void Update()
        {
            SetFillAmount();
        }

        public void SetFillAmount()
        {
            if (currHP != damageable.stats.GetStat(StatType.HP))
            {
                currHP = damageable.stats.GetStat(StatType.HP);
                maxHP = damageable.stats.GetStat(StatType.HP, StatSubValueType.Max);

                filling.fillAmount = (float) currHP / maxHP;
                
                if (currHP / maxHP >= 0.999f)
                {
                    white.gameObject.SetActive(false);
                    filling.gameObject.SetActive(false);
                }
                else
                {
                    white.gameObject.SetActive(true);
                    filling.gameObject.SetActive(true);
                }
            }
            white.fillAmount = Mathf.Lerp(white.fillAmount, (float) currHP / maxHP, lerpSpeed * Time.deltaTime);
            
            
        }
    }
}