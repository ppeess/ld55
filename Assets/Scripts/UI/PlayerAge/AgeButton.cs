﻿using System;
using Economy;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PlayerAge
{
    public class AgeButton : MonoBehaviour
    {
        public Button button;
        public GameObject boneHolder;
        public TMP_Text boneText;
        public GameObject giantBoneHolder;
        public TMP_Text giantBoneText;
        public GameObject crystalHolder;
        public TMP_Text crystalText;

        private bool _costsCrystals;
        private Resource _crystalCost;
        private bool _costsBones;
        private Resource _boneCost;
        private bool _costsGiantBones;
        private Resource _giantBoneCost;

        private Color notBuyable = Color.red;
        private Color buyable = Color.white;

        public ResourceManager resourceManager;
        public Stage.PlayerAge age;

        private void Start()
        {
            resourceManager = ResourceManager.instance;
        }

        private void Update()
        {
            if (_costsBones)
            {
                boneText.color = resourceManager.CanPurchase(_boneCost) ? buyable : notBuyable;
            }
            if (_costsGiantBones)
            {
                giantBoneText.color = resourceManager.CanPurchase(_giantBoneCost) ? buyable : notBuyable;
            }
            if (_costsCrystals)
            {
                crystalText.color = resourceManager.CanPurchase(_crystalCost) ? buyable : notBuyable;
            }
        }

        public void SetAge(Stage.PlayerAge futureAge)
        {
            if (futureAge == null) {
                gameObject.SetActive(false);
                return;
            }

            age = futureAge;
            gameObject.SetActive(true);
            
            button.image.sprite = age.icon;

            boneHolder.SetActive(false);
            giantBoneHolder.SetActive(false);
            crystalHolder.SetActive(false);

            foreach (Resource resource in age.cost)
            {
                switch (resource.resourceType)
                {
                    case ResourceType.Bone:
                        _costsBones = true;
                        _boneCost = resource;
                        boneText.text = _boneCost.amount.ToString();
                        boneHolder.SetActive(true);
                        break;
                    case ResourceType.GiantBone:
                        _costsGiantBones = true;
                        _giantBoneCost = resource;
                        giantBoneText.text = _giantBoneCost.amount.ToString();
                        giantBoneHolder.SetActive(true);
                        break;
                    case ResourceType.Crystal:
                        _costsCrystals = true;
                        _crystalCost = resource;
                        crystalText.text = _crystalCost.amount.ToString();
                        crystalHolder.SetActive(true);
                        break;
                }
            }

            button.onClick.AddListener(delegate()
            {
                AgeUp();
            });
        }

        public void AgeUp()
        {
            if (resourceManager.CanPurchase(age.cost))
            {
                resourceManager.Purchase(age.cost);
                GameManager.instance.AgeUp();   
            }
        }
    }
}