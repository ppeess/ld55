﻿using TMPro;
using UnityEngine;


namespace UI
{
    public class TimerDisplay : MonoBehaviour
    {
        public TMP_Text timeText;

        private float elapsedTime = 0f;

        private void Update()
        {
            elapsedTime += Time.deltaTime;
            UpdateTimeText();
        }

        private void UpdateTimeText()
        {
            int minutes = Mathf.FloorToInt(elapsedTime / 60f);
            int seconds = Mathf.FloorToInt(elapsedTime % 60f);

            timeText.text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        }
    }
}