﻿using System;
using Economy;
using Stage;
using TMPro;
using Units;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Building
{
    public class BuildButton : MonoBehaviour
    {
        public Button button;
        public GameObject boneHolder;
        public TMP_Text boneText;
        public GameObject giantBoneHolder;
        public TMP_Text giantBoneText;
        public GameObject crystalHolder;
        public TMP_Text crystalText;

        private bool _costsCrystals;
        private Resource _crystalCost;
        private bool _costsBones;
        private Resource _boneCost;
        private bool _costsGiantBones;
        private Resource _giantBoneCost;

        private Color notBuyable = Color.red;
        private Color buyable = Color.white;

        public ResourceManager resourceManager;

        public Lane lane;
        public UnitController unitController;

        private void Start()
        {
            resourceManager = ResourceManager.instance;
        }

        public void Initialize(Lane lane, UnitController unit)
        {
            unitController = unit;
            this.lane = lane;
            button.image.sprite = unitController.buildIcon;

            foreach (Resource resource in unit.unitCost)
            {
                switch (resource.resourceType)
                {
                    case ResourceType.Bone:
                        _costsBones = true;
                        _boneCost = resource;
                        boneText.text = _boneCost.amount.ToString();
                        boneHolder.SetActive(true);
                        break;
                    case ResourceType.GiantBone:
                        _costsGiantBones = true;
                        _giantBoneCost = resource;
                        giantBoneText.text = _giantBoneCost.amount.ToString();
                        giantBoneHolder.SetActive(true);
                        break;
                    case ResourceType.Crystal:
                        _costsCrystals = true;
                        _crystalCost = resource;
                        crystalText.text = _crystalCost.amount.ToString();
                        crystalHolder.SetActive(true);
                        break;
                }
            }

            button.onClick.AddListener(delegate()
            {
                BuyUnit();
            });
        }

        private void Update()
        {
            if (_costsBones)
            {
                boneText.color = resourceManager.CanPurchase(_boneCost) ? buyable : notBuyable;
            }
            if (_costsGiantBones)
            {
                giantBoneText.color = resourceManager.CanPurchase(_giantBoneCost) ? buyable : notBuyable;
            }
            if (_costsCrystals)
            {
                crystalText.color = resourceManager.CanPurchase(_crystalCost) ? buyable : notBuyable;
            }
        }

        private void BuyUnit()
        {
            if (resourceManager.CanPurchase(unitController.unitCost))
            {
                resourceManager.Purchase(unitController.unitCost);
                lane.Spawn(unitController);   
            }
        }
    }
}