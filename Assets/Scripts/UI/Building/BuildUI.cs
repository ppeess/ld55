﻿using System;
using Stage;
using Units;
using UnityEngine;

namespace UI.Building
{
    public class BuildUI : MonoBehaviour
    {
        public GameObject canvasHolder;
        public GameObject buttonHolder;
        public bool isActive = false;

        public GameManager gameManager;
        public Lane lane;
        
        private void Start()
        {
            canvasHolder = transform.Find("Canvas").gameObject;
            canvasHolder.gameObject.SetActive(isActive);
        }

        public void ActivateUI(Lane lane)
        {
            isActive = true;
            canvasHolder = transform.Find("Canvas").gameObject;            
            canvasHolder.gameObject.SetActive(isActive);
            gameManager = GameManager.instance;
            this.lane = lane;
            DrawBuildUI();
        }

        public void DrawBuildUI()
        {
            foreach (Transform transform in buttonHolder.transform)
            {
                Destroy(transform.gameObject);
            }
            
            gameManager = GameManager.instance;

            foreach (UnitController unitController in gameManager.availableUnits)
            {
                GameObject unitButton = Instantiate(gameManager.uiHolder.GetUiElement(UiAssignmentValue.BuildButton), buttonHolder.transform, false);
                BuildButton button = unitButton.GetComponentInChildren<BuildButton>();
                button.Initialize(lane, unitController);
            }
        }
    }
}