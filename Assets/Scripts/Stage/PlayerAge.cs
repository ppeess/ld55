﻿using System.Collections.Generic;
using Economy;
using Units;
using UnityEngine;

namespace Stage
{
    [CreateAssetMenu(fileName = "Age", menuName = "Ages/Player Age")]
    public class PlayerAge : ScriptableObject
    {
        public Sprite icon;
        public List<UnitController> availableUnits;
        public List<Resource> cost;
        public int boneIncome;
        public int giantBoneIncome;
        public int crystalIncome;
    }
}