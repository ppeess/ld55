﻿using System.Collections;
using System.Collections.Generic;
using Stage.Movement.WaveManagement;
using UI.Building;
using Units;
using UnityEngine;
using Utils;
using Wave = Stage.WaveManagement.Wave;

namespace Stage
{
    [System.Serializable]
    public class Lane
    {
        public List<Target> allyTargets;
        public List<Target> enemyTargets;
        public List<Wave> possibleWaves;
        public Wave fallbackWave;

        public EnemyAge currentAge;
        public List<EnemyAge> enemyAges;
        public float timeToNextAge = 100f;
        
        public int currentResources = 150;
        public int resourcesPerFive = 10;
        public int resourceIncreasePerMinute = 5;
        public float resourceCooldown;
        public float resourceIncreaseCooldown;

        public float remainingSpawnCooldown = 15f;
        public bool laneWon;
        
        private Stage stage;

        public void Initialize(Stage stage)
        {
            this.stage = stage;
            resourceCooldown = 5f;
            resourceIncreaseCooldown = 60f;
            Initialize();
            possibleWaves = currentAge.possibleWaves;
            fallbackWave = currentAge.fallbackWave;
            timeToNextAge = currentAge.timeUntilNextAge;

            enemyTargets[0].GetComponentInChildren<BuildUI>().ActivateUI(this);
        }

        public void UpdateAge()
        {
            currentAge = enemyAges[0];
            possibleWaves = currentAge.possibleWaves;
            fallbackWave = currentAge.fallbackWave;
            enemyAges.RemoveAt(0);
            timeToNextAge = currentAge.timeUntilNextAge;
        }
        
        public void Update(float deltaTime)
        {
            resourceCooldown -= deltaTime;
            if (resourceCooldown <= 0)
            {
                currentResources += resourcesPerFive;
                resourceCooldown = 5f;
            }
            
            resourceIncreaseCooldown -= deltaTime;
            if (resourceIncreaseCooldown <= 0)
            {
                resourcesPerFive += resourceIncreasePerMinute;
                resourceIncreaseCooldown = 60f;
            }
            
            remainingSpawnCooldown -= deltaTime;
            if (remainingSpawnCooldown <= 0)
            {
                SpawnNewWave();
            }

            if (enemyAges.Count > 0)
            {
                timeToNextAge -= Time.deltaTime;
                if (timeToNextAge <= 0)
                {
                    UpdateAge();
                }
            }
        }
        
        public void SpawnNewWave()
        {
            // Select wave
            Wave waveToSpawn = null;
            int currentCost = 0;
            foreach (Wave toCheck in possibleWaves)
            {
                if (toCheck.CanSpawnWave(currentResources))
                {
                    if (toCheck.cost > currentCost)
                    {
                        waveToSpawn = toCheck;
                        currentCost = toCheck.cost;
                    }
                }
            }
            
            // Fallback if no resources available
            if (waveToSpawn == null)
            {
                waveToSpawn = fallbackWave;
            }

            currentResources -= waveToSpawn.cost;
            remainingSpawnCooldown = waveToSpawn.delayAfterWave;
            stage.StartCoroutine(SpawnWave(waveToSpawn));
        }

        IEnumerator SpawnWave(Wave wave)
        {
            if (wave != null)
            {
                foreach (SpawnData data in wave.spawnData)
                {
                    int remainingUnits = data.numberOfUnits;

                    while (remainingUnits > 0)
                    {
                        yield return new WaitForSeconds(data.delayBeforeUnit);
                        if (laneWon)
                        {
                            break;
                        }
                        Spawn(data.toSpawn);
                        remainingUnits -= 1;
                    }
                    
                    if (laneWon)
                    {
                        break;
                    }
                }
            }
        }
        
        public void Spawn(UnitController unitController)
        {
            Target target = GetCurrentSpawn(unitController.faction);
            Transform spawnLocation = target.spawnPoints[Random.Range(0, target.spawnPoints.Count)];
            GameObject unit = GameObject.Instantiate(unitController.gameObject, spawnLocation.position, Quaternion.identity);
            unit.GetComponent<UnitController>().Initialize(this);
        }
        
        public void Initialize()
        {
            foreach (Target target in allyTargets)
            {
                target.subscribers.Add(this);   
            }
            foreach (Target target in enemyTargets)
            {
                target.subscribers.Add(this);   
                target.InitUI();
            }
        }

        public void TargetDestroyed(Target target)
        {
            if (target.faction == Faction.Ally)
            {
                enemyTargets.Remove(target);
                if (enemyTargets.Count > 0)
                {
                    enemyTargets[0].GetComponentInChildren<BuildUI>().ActivateUI(this);
                }
            }
            else
            {
                allyTargets.Remove(target);
            }
        }

        public Target GetNextTarget(Faction faction)
        {
            if (faction == Faction.Enemy)
            {
                if (enemyTargets.Count <= 0)
                {
                    return null;
                }
                return enemyTargets[0];
            }
            if (allyTargets.Count <= 0)
            {
                return null;
            }
            return allyTargets[0];
        }

        public Target GetCurrentSpawn(Faction faction)
        {
            if (faction == Faction.Ally)
            {
                if (enemyTargets.Count <= 0)
                {
                    return null;
                }
                return enemyTargets[0];
            }
            if (allyTargets.Count <= 0)
            {
                return null;
            }
            return allyTargets[0];
        }
    }
}