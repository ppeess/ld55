﻿using System;
using Units;

namespace Stage.Movement.WaveManagement
{
    [Serializable]
    public class SpawnData
    {
        public float delayBeforeUnit = 0.5f;
        public int numberOfUnits = 1;
        public UnitController toSpawn;
    }
}