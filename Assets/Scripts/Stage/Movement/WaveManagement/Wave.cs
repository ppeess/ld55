﻿using System.Collections.Generic;
using Stage.Movement.WaveManagement;
using UnityEngine;

namespace Stage.WaveManagement
{
    [CreateAssetMenu(fileName = "Waves", menuName = "Wave")]
    public class Wave : ScriptableObject
    {
        public List<SpawnData> spawnData;
        public int cost;
        public float delayAfterWave;

        public bool CanSpawnWave(int resource)
        {
            return resource >= cost;
        }
    }
}

public enum WaveType
{
    Normal,
    Large,
    Boss
}