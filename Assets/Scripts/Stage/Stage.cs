﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Stage
{
    [System.Serializable]
    public class Stage : MonoBehaviour
    {
        public List<Lane> lanes;
        public List<Target> loseCondition;
        public List<Target> winCondition;

        private void Start()
        {
            foreach (Lane lane in lanes)
            {
                lane.Initialize(this);
            }
        }

        private void Update()
        {
            foreach (Lane lane in lanes)
            {
                if (!lane.laneWon)
                {
                    lane.Update(Time.deltaTime);
                }
            }

            // Lich was destroyed =(
            if (loseCondition.FindAll(x => x == null).Count > 0)
            {
                GameManager.instance.Lose();
            }
            
            // Enemy camps were destroyed >=)
            if (winCondition.Find(x => x != null) == null)
            {
                GameManager.instance.Win();
            }
        }
    }
}