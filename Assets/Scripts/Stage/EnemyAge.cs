﻿using System.Collections.Generic;
using Economy;
using Stage.WaveManagement;
using Units;
using UnityEngine;

namespace Stage
{
    [CreateAssetMenu(fileName = "Age", menuName = "Ages/Enemy Age")]
    public class EnemyAge : ScriptableObject
    {
        public List<Wave> possibleWaves;
        public Wave fallbackWave;
        public float timeUntilNextAge = 60f;
    }
}