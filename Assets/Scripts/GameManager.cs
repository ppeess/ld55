using System.Collections.Generic;
using System.Linq;
using Economy;
using Stage;
using UI;
using UI.Building;
using UI.PlayerAge;
using Units;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Settings _settings;
    public UIHolder uiHolder;
    public CameraController cameraController;
    
    public static GameManager instance;

    public List<UnitController> availableUnits;

    public GameObject winUi;
    public GameObject loseUi;
    public GameObject pauseMenu;

    public PlayerAge currentAge;
    public List<PlayerAge> futureAges;
    public AgeButton ageButton;
    
    public void Pause(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        bool paused = pauseMenu.activeSelf;
        pauseMenu.SetActive(!paused);
        if (paused)
        {
            Time.timeScale = 1f;
        }
        else
        {
            Time.timeScale = 0f;
        }
    }
    
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
    
    private void Awake()
    {
        Time.timeScale = 1f;
        instance = this;
        cameraController = FindFirstObjectByType<CameraController>();
        ageButton.SetAge(futureAges[0]);
        availableUnits = currentAge.availableUnits;
    }
    
    void Start()
    {
        Settings.Initialize();
        winUi.SetActive(false);
        loseUi.SetActive(false);
        
        ResourceManager.instance.bonesIncome = currentAge.boneIncome;
        ResourceManager.instance.giantBonesIncome = currentAge.giantBoneIncome;
        ResourceManager.instance.crystalIncome = currentAge.crystalIncome;
    }

    public void Win()
    {
        Debug.Log("Won!");
        winUi.SetActive(true);
        Time.timeScale = 0f;
    }
    
    public void Lose()
    {
        Debug.Log("Lost!");
        loseUi.SetActive(true);
        Time.timeScale = 0f;
    }

    public void AgeUp()
    {
        currentAge = futureAges[0];
        availableUnits = currentAge.availableUnits;
        ResourceManager.instance.bonesIncome = currentAge.boneIncome;
        ResourceManager.instance.giantBonesIncome = currentAge.giantBoneIncome;
        ResourceManager.instance.crystalIncome = currentAge.crystalIncome;
        
        List<BuildUI> buildUis = FindObjectsByType<BuildUI>(FindObjectsSortMode.None).ToList();
        foreach (BuildUI buildUi in buildUis)
        {
            buildUi.DrawBuildUI();
        }
        
        futureAges.RemoveAt(0);
        if (futureAges.Count > 0)
        {
            ageButton.SetAge(futureAges[0]);
        }
        else
        {
            ageButton.SetAge(null);
        }
    }
}
